import math

from data_scructure import NeighbourList


def debug(fun):
    def wrapper(obj, *args, **kwargs):
        result = fun(obj, *args, **kwargs)
        print(obj.vertexes)
        return result
    return wrapper


class Solver:

    def __init__(self, circuit):
        self.circuit = circuit
        self.start = circuit.get_any()
        self.circuit.mark_as_visited(self.start)
        self.is_first_step = True
        self.is_line = False
        self.vertexes = []

    def check_angle(self, cell):
        """
        Проверяем, есть ли угол в заданном квадрате 3х3
        :param cell: центр квадрата
        :return:
        """
        neighbour_count = neighbour_sum = 0
        neighbour_angle = False
        for i in NeighbourList(cell):
            if self.circuit.exists(i):
                if self.circuit.is_angle(i):
                    neighbour_angle = True
                neighbour_count += 1
                neighbour_sum += i - cell
        # рядом 2 углов быть не может
        if neighbour_angle:
            if neighbour_count > 2:
                self.circuit.mark_as_angle(cell)
            return False
        # мало соседей - фигура не замкнута
        if neighbour_count < 2:
            self.is_line = True
            return False
        # 2 соседа - смотрим их расположение
        elif neighbour_count == 2:
            return abs(neighbour_sum.real) + abs(neighbour_sum.imag) > 1
        # много соседей - 100% угол
        return True

    def dfs(self, cell):
        for n in NeighbourList(cell):
            if n == self.start:
                if self.is_first_step:
                    self.is_first_step = False
                else:
                    return True
            if self.circuit.not_visited(n):
                self.circuit.mark_as_visited(n)
                if self.check_angle(n):
                    self.circuit.mark_as_angle(n)
                    self.vertexes.append(n)
                if self.dfs(n):
                    return True

    @staticmethod
    def same_num(len1, len2, delta=0.01):
        return abs(len1 - len2) / len1 < delta

    @staticmethod
    def same_distance(first, second):
        len1 = abs(first[0] - first[1])
        len2 = abs(second[0] - second[1])
        return Solver.same_num(len1, len2)

    def triangle_class(self):
        modifiers = []
        lengths = [abs(self.vertexes[0] - self.vertexes[1]),
                   abs(self.vertexes[2] - self.vertexes[1]),
                   abs(self.vertexes[0] - self.vertexes[2]), ]
        equal_sides = sum(map(lambda x: int(Solver.same_num(x[0], x[1], 0.05)), (
            (lengths[0], lengths[1]),
            (lengths[0], lengths[2]),
            (lengths[2], lengths[1]),
        )))
        if equal_sides == 1:
            modifiers.append('равнобедренный')
        elif equal_sides == 3:
            modifiers.append('равносторонний')

        # Пифагор
        hyp = max(lengths)
        cat = math.sqrt(sum(map(lambda x: x**2, (i for i in lengths if i != hyp))))
        if Solver.same_num(hyp, cat, 0.03):
            modifiers.append('прямоугольный')

        return ' '.join(modifiers + ['треугольник'])

    def circle_check(self):
        min_x = min(self.circuit._data, key=lambda x: x.real).real
        max_x = max(self.circuit._data, key=lambda x: x.real).real
        min_y = min(self.circuit._data, key=lambda x: x.imag).imag
        max_y = max(self.circuit._data, key=lambda x: x.imag).imag
        center = complex(round((min_x + max_x)/2), round((min_y+max_y)/2))
        radius = abs(center - self.circuit.get_any())
        for i in self.circuit._data:
            if not Solver.same_num(radius, abs(i-center), 0.05):
                return False
        return True

#    @debug
    def solve(self):
        if not self.dfs(self.start) or self.is_line:
            if len(self.vertexes) > 0:
                return 'ломаная'
            return 'линия'
        if len(self.vertexes) == 0:
            if self.circle_check():
                return 'круг'
            return 'овал'
        if len(self.vertexes) == 3:
            return self.triangle_class()
        elif len(self.vertexes) == 4:
            if self.same_distance(self.vertexes[0:2], self.vertexes[1:3]):
                return 'квадрат'
            return 'прямоугольник'
        else:
            return '????????????????'

