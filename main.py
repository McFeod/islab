from bmp_reader import read_from_path
from solver import Solver


def main():
    for i in range(1, 15):
        print("{}: {}".format(i, process_image("tests/test{}.bmp".format(i))))


def process_image(path):
    circuit = read_from_path(path)
    return Solver(circuit).solve()


if __name__ == '__main__':
    main()
