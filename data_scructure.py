class NeighbourList:
    _order = (1, 1+1j, 1j, -1+1j, -1, -1-1j, -1j, 1-1j)

    def __init__(self, cell):
        self.start = cell

    def __iter__(self):
        self.iterator = NeighbourList._order.__iter__()
        return self

    def __next__(self):
        return self.start + next(self.iterator)


class Circuit:
    VISITED = 1
    NOT_VISITED = 2
    ANGLE = 3

    def __init__(self):
        self._data = {}

    def exists(self, cell):
        return cell in self._data

    def not_visited(self, cell):
        return self.exists(cell) and self._data[cell] == self.NOT_VISITED

    def is_angle(self, cell):
        return self.exists(cell) and self._data[cell] == self.ANGLE

    def mark_as_visited(self, cell):
        if self.exists(cell):
            self._data[cell] = self.VISITED

    def mark_as_angle(self, cell):
        if self.exists(cell):
            self._data[cell] = self.ANGLE

    def add_point(self, cell):
        if not self.exists(cell):
            self._data[cell] = self.NOT_VISITED

    def get_any(self):
        return self._data.__iter__().__next__()

