from PIL import Image

from data_scructure import Circuit


def read_from_path(filename):
    with Image.open(filename) as fil:
        width = fil.size[0]
        result = Circuit()
        for i, px in enumerate(fil.getdata()):
            if px != 0:
                result.add_point(complex(i % width, i // width))
        return result


